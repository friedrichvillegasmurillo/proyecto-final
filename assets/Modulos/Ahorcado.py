import time
from tkinter import *
from random import randint
import re
from unicodedata import normalize
from tkinter.messagebox import *


def textreader():
	filereader = open("diccionary.txt", "r")
	reader = filereader.read()
	#print (reader)
textreader()

def splitercaller():
    global filereader
    global reader
    global readerspliter
    filereader = open("diccionary.txt", "r")
    reader = filereader.read()
    readerspliter = reader.split()
    lenreader = len(readerspliter)
    for i in range(0,lenreader):
        readernormalize = readerspliter[i]
        readernormalize = re.sub(
        r"([^n\u0300-\u036f]|n(?!\u0303(?![\u0300-\u036f])))[\u0300-\u036f]+", r"\1", 
        normalize( "NFD", readernormalize), 0, re.I
        )
        #readernormalize = normalize( 'NFC', readernormalize)
        #print(readernormalize)
        readerspliter[i] = readernormalize 
splitercaller()

def global_argument():
    global randword
    global wordtaker
    randword=randint(0,len(readerspliter)-1)
    wordtaker = readerspliter[randword].lower()
global_argument()

print(wordtaker)

handwritinglist = []

lifes =7
wins = 0
def FunctionSrs():
    global lifes 
    global wins
    handwritinglist.append(gethandwriting.get())
    print(handwritinglist)
    label_latter[ord(gethandwriting.get())-97].config(text="")
    if gethandwriting.get() in wordtaker:
        if wordtaker.count(gethandwriting.get()) > 1:
            wins += wordtaker.count(gethandwriting.get())
            for i in range(len(wordtaker)):
                if wordtaker[i] == gethandwriting.get():
                    lowbar[i].config(text=""+gethandwriting.get())
        else:
            wins+=1
            lowbar[wordtaker.index(gethandwriting.get())].config(text=""+gethandwriting.get())
        if wins == len(wordtaker):
            showwarning(title="Ganaste",message="Lo lograste!")
    else:
        lifes -=1
        Label(gameframe,text="Vidas: {}".format(lifes), font = ("Arial" , 20, "normal")).grid(row = 0 , column = 3, padx =7, pady = 7)
        print(lifes)
        if lifes == 0:
            showwarning(title="Perdiste", message= "Ya no tienes vidas")

root = Tk()
gethandwriting = StringVar()
root.config(width = 800, height = 600)
gameframe = Frame(root)
gameframe.config(width = 800, height = 600, relief = "sunken")
gameframe.grid_propagate(False)
gameframe.pack()

Label(gameframe, text="El Ahorcado", font = ("Arial" , 30, "normal"))
Label(gameframe, text="Introduzca una letra: ", font = ("Arial" , 20, "normal")).grid(row = 0 , column = 0, padx =7, pady = 7)

Keyreader = Entry(gameframe, width= 2, font = ("Arial", 20, "normal"), textvariable = gethandwriting).grid(row = 0 , column = 1, padx =7, pady = 7)

botonaction = Button(gameframe, text="intentar", command = FunctionSrs).grid(row = 1 , column = 1, padx =7, pady = 7)

label_latter = [Label(gameframe, text=chr(j+97), font = ("Arial", 20, "normal"))for j in range(26)]


lowbar = [Label(gameframe, text = "_", font=("Arial", 20, "normal")) for i in wordtaker]

def letterplacer():
    x=50
    y=150
    counter = 0
    Label(gameframe, text="Sus letras sin usar").place(x=50, y=100)
    for i in range(26):
        counter+=1
        label_latter[i].place(x=x,y=y)
        x+=30
        if counter==5:
            y+=50
            contador=0
            x=50
letterplacer()
placex=250
for i in range(len(wordtaker)):
    lowbar[i].place(x=placex, y=500)
    placex+=50

root.mainloop()